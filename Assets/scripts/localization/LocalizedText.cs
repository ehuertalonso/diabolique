﻿using UnityEngine;
using UnityEngine.UI;

public class LocalizedText : MonoBehaviour
{

    public string key;

    // Use this for initialization
    void Start()
    {
        Text text = GetComponent<Text>();
        text.text = LocalizationManager.GetLocalizedValue(key);
    }

    public string getText() {
        return GetComponent<Text>().text;
    }

}