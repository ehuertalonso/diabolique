﻿using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LocalizationManager : MonoBehaviour
{

    private static LocalizationManager instance;

    private static Dictionary<string, string> localizedText;
    private static bool isReady = false;
    private static string missingTextString = "";


    // Use this for initialization
    void Start()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
        LoadLocalizedText();
        DontDestroyOnLoad(gameObject);
    }

    public static void LoadLocalizedText()
    {
        string dataAsJson;
        string filePath = Path.Combine(Application.streamingAssetsPath,
            (SystemLanguage.Spanish.Equals(Application.systemLanguage) ? SystemLanguage.Spanish.ToString() : SystemLanguage.English.ToString()) + ".json");
        if (Application.platform == RuntimePlatform.Android)
        {
            WWW reader = new WWW(filePath);
            while (!reader.isDone) { }

            dataAsJson = reader.text;
        }
        else
        {
            dataAsJson = File.ReadAllText(filePath);
        }

        localizedText = getLocalizedTextFromJSON(dataAsJson);
        
        Debug.Log("Data loaded, dictionary contains: " + localizedText.Count + " entries");

        SceneManager.LoadScene("welcome");

        isReady = true;
    }

    private static Dictionary<string, string> getLocalizedTextFromJSON(string dataAsJson)
    {
        Dictionary<string, string> toReturn = new Dictionary<string, string>();
        LocalizationData loadedData = JsonUtility.FromJson<LocalizationData>(dataAsJson);

        for (int i = 0; i < loadedData.items.Length; i++)
        {
            toReturn.Add(loadedData.items[i].key, loadedData.items[i].value);
        }

        return toReturn;
    }

    public static string GetLocalizedValue(string key)
    {
        if (!isReady)
        {
            LoadLocalizedText();
        }
        string result = missingTextString;
        if (localizedText.ContainsKey(key))
        {
            result = localizedText[key];
        }

        return result;

    }

}