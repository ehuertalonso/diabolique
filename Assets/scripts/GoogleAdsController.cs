﻿using GoogleMobileAds.Api;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoogleAdsController : MonoBehaviour
{

    private BannerView bannerView;
    private InterstitialAd interstitial;
    private Hermes hermes;
    // TODO: TEST DATA REPLACE WITH REAL VALUES BEFORE DEPLOYMENT
    private readonly string appId = "ca-app-pub-7909878585909867~9875625568";
    private readonly string bannerUnitId = "ca-app-pub-7909878585909867/4903743757";
    private readonly string interstitialUnitId = "ca-app-pub-7909878585909867/6792089238";
    // Use this for initialization
    void Start()
    {
        MobileAds.Initialize(appId);

        this.RequestBanner();
        this.RequestInterstitial();
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "RemoveBanner");
        hermes.AddObserver(this, "RemoveInterstitial");
        hermes.AddObserver(this, "RequestBanner");
        hermes.AddObserver(this, "RequestInterstitial");
        hermes.AddObserver(this, "ShowAdEndOfGame");
    }

    // Update is called once per frame
    void Update()
    {

    }

    void RemoveBanner()
    {
        if (bannerView != null)
        {
            bannerView.Destroy();
        }
    }

    void RemoveInterstitial()
    {
        if (interstitial != null)
        {
            interstitial.Destroy();
        }
    }

    void RequestBanner()
    {
        RemoveBanner();
        // Create a 320x50 banner at the top of the screen.
        bannerView = new BannerView(bannerUnitId, AdSize.Banner, AdPosition.Top);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();

        // Load the banner with the request.
        bannerView.LoadAd(request);
    }

    void RequestInterstitial()
    {
        RemoveInterstitial();
        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(interstitialUnitId);

        // Create an empty ad request.
        AdRequest request = new AdRequest.Builder().Build();
        // Load the interstitial with the request.
        interstitial.LoadAd(request);
    }

    void ShowAdEndOfGame()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }

}