﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour {

    private static bool created = false;
    private Hermes hermes;
    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else Destroy(gameObject);
    }

    private void Start()
    {
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "LoadLevel");
    }

    private void Update()
    {

    }

    void LoadLevel(Notification notification)
    {
        string level = (string)notification.data;
        Debug.Log("Se procede a cargar el nivel " + level);
        SceneManager.LoadSceneAsync(level);
    }

}
