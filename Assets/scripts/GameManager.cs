﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
    public GameObject playerTemplate;
    public GameObject enemyTemplate;
    public Text levelText;
    public WorldLimits worldLimits;
    public Text showScores;

    // Settings controlled by the difficulty of the game(default to difficulty normal)
    public float velocityIncreaseFactorPerLevel = 0.02f;
    public float enemiesSpawnPerLevel = 1;
    public int maxLevelTime = 5;
    public int pointsPerLevel = 100;
    public float pointScaleFactorPerTenLevels = 1.5f;
    public float scorePerTimeFactor = 1;


    public struct WorldLimits
    {
        public float limitUp;
        public float limitBottom;
        public float limitLeft;
        public float limitRight;
    };

    private float enemySpawnDistanceToPlayerThreshold = 3f;
    private int fingerId;
    private bool touched = false;
    private float enemiesNumber = 5;
    private int level = 1;
    private int levelTime = -1;
    private Vector3 lastPosition;
    private NageruController player;
    private Rigidbody2D rb;
    private Transform enemiesParent;
    private float enemyVelocityIncrease = 0f;

    private GameState gs;
    private Camera myCamera;
    private Transform upPosition;
    private Transform bottomPosition;
    private Transform leftPosition;
    private Transform rightPosition;
    private Hermes hermes;
    private static bool created = false;

    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else Destroy(gameObject);
    }


    // Use this for initialization
    void Start() {

        // Find permanent elements
        gs = FindObjectOfType<GameState>();
        showScores = GameObject.FindGameObjectWithTag("showScore").GetComponentInChildren<Text>();

        // Initialize Hermes, the ultimate power of message notification! Hail to the Hermes!!
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "LoadSceneLoaded");
        hermes.AddObserver(this, "StartGame");
        hermes.AddObserver(this, "EndGame");
        hermes.AddObserver(this, "EnableScoreButton");
        hermes.AddObserver(this, "DisableScoreButton");

        Debug.Log("Lenguaje: " + Application.systemLanguage);
    }

    void EnableScoreButton()
    {
        if(!showScores) showScores = GameObject.FindGameObjectWithTag("showScore").GetComponentInChildren<Text>();
        showScores.color = Color.black;
    }
    
    void DisableScoreButton()
    {
        if (!showScores) showScores = GameObject.FindGameObjectWithTag("showScore").GetComponentInChildren<Text>();
        showScores.color = Color.grey;
    }

    void LoadSceneLoaded()
    {
        SceneManager.sceneLoaded += EnableButtons;
    }

    void EnableButtons(Scene scene, LoadSceneMode mode)
    {
        SceneManager.sceneLoaded -= EnableButtons;
        showScores = GameObject.FindGameObjectWithTag("showScore").GetComponentInChildren<Text>();
        if (Social.localUser.authenticated) showScores.color = Color.black;
        else showScores.color = Color.grey;
    }

    public void StartGame()
    {
        SceneManager.sceneLoaded += PlaySceneLoaded;
    }

    void PlaySceneLoaded(Scene scene, LoadSceneMode mode) {
        SceneManager.sceneLoaded -= PlaySceneLoaded;
        myCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();

        upPosition = GameObject.FindWithTag("upBoundary").transform;
        bottomPosition = GameObject.FindWithTag("bottomBoundary").transform;
        leftPosition = GameObject.FindWithTag("leftBoundary").transform;
        rightPosition = GameObject.FindWithTag("rightBoundary").transform;

        // Set up correct world limits for the screen
        float up = myCamera.ScreenToWorldPoint(new Vector3(0, myCamera.pixelHeight, 1)).y;
        upPosition.position = new Vector3(upPosition.position.x, up, 0f);

        float bottom = -1 * myCamera.ScreenToWorldPoint(new Vector3(0, myCamera.pixelHeight, 1)).y;
        bottomPosition.position = new Vector3(bottomPosition.position.x, bottom, 0f);

        float left = -1 * myCamera.ScreenToWorldPoint(new Vector3(myCamera.pixelWidth, 0, 1)).x;
        leftPosition.position = new Vector3(left, leftPosition.position.y, 0f);

        float right = myCamera.ScreenToWorldPoint(new Vector3(myCamera.pixelWidth, 0, 1)).x;
        rightPosition.position = new Vector3(right, rightPosition.position.y, 0f);

        worldLimits = new WorldLimits()
        {
            limitUp = up,
            limitBottom = bottom,
            limitLeft = left,
            limitRight = right
        };

        // Object to hold the enemies
        var obj = new GameObject();
        obj.transform.position = new Vector3(0, 0, -0.5f);
        enemiesParent = obj.transform;

        // Start music
        AudioOptions options = new AudioOptions()
        {
            resource = "music/Playmusic",
            volume = gs.gameData.musicVolume
        };
        hermes.PostNotification(this, "PlayMusic", options);
        
        // Initializing the game
        enemiesNumber = 5;
        level = 1;
        levelTime = -1;
        velocityIncreaseFactorPerLevel = 0.02f;
        enemiesSpawnPerLevel = 1;
        maxLevelTime = 5;
        pointsPerLevel = 100;
        pointScaleFactorPerTenLevels = 2;
        scorePerTimeFactor = 1;
        gs.StartGame();
        IncreaseParametersWithDifficulty();
        Init();

        if (player != null)
        {
            rb = player.GetComponent<Rigidbody2D>();
        }
    }

    public void Update()
    {
        if (Input.touchCount == 0 && touched)
        {
            touched = false;
            fingerId = -1;
        }
        else if (Input.touchCount > 0 && !touched)
        {
            fingerId = Input.GetTouch(0).fingerId;
            touched = true;
            lastPosition = Input.GetTouch(0).position;
            lastPosition = myCamera.ScreenToWorldPoint(lastPosition);
        }
    }

    public void FixedUpdate() {

            for (int x = 0; x < Input.touchCount; x++)
            {
            if (!touched)
            {
                if (Input.GetTouch(x).phase == TouchPhase.Began)
                {
                    fingerId = Input.GetTouch(x).fingerId;
                    touched = true;
                    lastPosition = Input.GetTouch(x).position;
                    lastPosition = myCamera.ScreenToWorldPoint(lastPosition);
                    break;
                }
            }
            else
            {
                if (fingerId == Input.GetTouch(x).fingerId)
                {
                    if (Input.GetTouch(x).phase == TouchPhase.Moved)
                    {
                        Vector3 newPos = myCamera.ScreenToWorldPoint(Input.GetTouch(x).position);
                        Vector3 newposRb = (Vector3)rb.position + newPos - lastPosition;
                        rb.MovePosition(new Vector3(Mathf.Clamp(newposRb.x, leftPosition.position.x, rightPosition.position.x), 
                            Mathf.Clamp(newposRb.y, bottomPosition.position.y, upPosition.position.y)));
                        lastPosition = newPos;
                    }
                    else if (Input.GetTouch(x).phase == TouchPhase.Ended || Input.GetTouch(x).phase == TouchPhase.Canceled)
                    {
                        touched = false;
                        fingerId = -1;
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
    }

    public void CleanScreen() {
        //GameObject enemies = GameObject.Find("Enemies");
        foreach (Transform t in enemiesParent) {
            Destroy(t.gameObject);
        }
    }

    public void Init() {
        levelTime = maxLevelTime;
        //Limpieza de pantalla
        CleanScreen();
        //Creacion del jugador
        CreatePlayer();
        //Creación de player y enemies
        CreateEnemies();
        //Programamos el tiempo disponible antes del siguiente nivel
        Invoke("DecreaseTime", 1);
    }

    public void DecreaseTime() {
        levelTime--;
        if (levelTime <= 0) {
            IncreaseLevel();
        }
        else {
            Invoke("DecreaseTime", 1);
        }
    }

    public void IncreaseLevel() {
        gs.AddScore(pointsPerLevel);
        level++;
        
        enemiesNumber += enemiesSpawnPerLevel;
        levelTime = maxLevelTime;
        if (level % 10 == 0) pointsPerLevel =Mathf.FloorToInt(pointsPerLevel * pointScaleFactorPerTenLevels);
        // Create new enemies
        for (int i = 0;i<enemiesSpawnPerLevel;i++) CreateEnemy();
        IncreaseEnemiesVelocity();

        // Start counting to new level
        Invoke("DecreaseTime", 1);
    }
    
    public void IncreaseParametersWithDifficulty()
    {
        if(gs.gameData.difficulty == 0) // LOW (reduce difficulty by a factor of 20%)
        {
            enemiesSpawnPerLevel = 0.8f;
            maxLevelTime *= 2;
            pointScaleFactorPerTenLevels *= 0.8f;
            velocityIncreaseFactorPerLevel *= 0.8f;
            scorePerTimeFactor *= 0.8f;
        }
        else if (gs.gameData.difficulty == 1) // MEDIUM
        {
            // Default parameters according to design are applied
        }
        else // HIGH (Increase difficulty by a factor of 20%)
        {
            enemiesSpawnPerLevel = 1.2f;
            maxLevelTime /= 2;
            pointScaleFactorPerTenLevels *= 1.2f;
            velocityIncreaseFactorPerLevel *= 1.2f;
            scorePerTimeFactor *= 1.2f;
        }
    }

    public float GetScorePerTimeFactor()
    {
        return scorePerTimeFactor;
    }

    private void IncreaseEnemiesVelocity()
    {
        //enemyVelocityIncrease += velocityIncreasePerLevel;
        foreach (EnemyController enemy in enemiesParent.GetComponentsInChildren<EnemyController>())
        {
            enemyVelocityIncrease = enemy.velocity * velocityIncreaseFactorPerLevel;
            enemy.IncreaseVelocity(enemyVelocityIncrease);
        }
    }

    private void CreatePlayer()
    {
        GameObject p = Instantiate(playerTemplate, new Vector3(0f, 0f, -0.5f), Quaternion.identity) as GameObject;
        player = p.GetComponent<NageruController>();
        p.layer = LayerMask.NameToLayer("Player");
    }

    private void CreateEnemies() {
        //Instanciamos los enemigos alrededor del jugador
        for (int x = 0; x < enemiesNumber; x++) {
            CreateEnemy();
        }
    }

    private void CreateEnemy()
    {
        bool okCoords = false;
        float newX = 0f;
        float newY = 0f;
        Vector3 v = Vector3.zero;
        while (!okCoords)
        {
            newX = UnityEngine.Random.Range(leftPosition.position.x, rightPosition.position.x);
            newY = UnityEngine.Random.Range(upPosition.position.y, bottomPosition.position.y);
            v = new Vector3(newX, newY, -0.5f);
            if(player!=null)
                okCoords = Distance(player.transform.position, v) > enemySpawnDistanceToPlayerThreshold;
            else // for testing only
                okCoords = v.magnitude > 1.5f;
        }
        GameObject e = Instantiate(enemyTemplate, v, Quaternion.identity) as GameObject;
        e.GetComponent<EnemyController>().IncreaseVelocity(enemyVelocityIncrease);
        e.transform.parent = enemiesParent;
    }

    private float Distance(Vector3 a, Vector3 b)
    {
        float deltaX = Mathf.Abs(a.x - b.x);
        float deltaY = Mathf.Abs(a.y - b.y);

        return (deltaX * deltaX + deltaY * deltaY); // calculate square hyptothenuse
    }

    public int CurrentLevel()
    {
        return level;
    }

    public GameState GetGameState()
    {
        return gs;
    }

    public void EndGame()
    {
        CancelInvoke("DecreaseTime");
        hermes.PostNotification(this, "LoadLevel", "lose");
        gs.EndGame();
        gs.SetMaxScore();
        hermes.PostNotification(this, "StopMusic");
        AudioOptions options = new AudioOptions()
        {
            resource = "music/Lose",
            volume = gs.gameData.effectsVolume
        };
        hermes.PostNotification(this, "PlayEffect", options);

    }
}
