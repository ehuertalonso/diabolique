﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsManager : MonoBehaviour {


    public Slider musicVolume;
    public Slider effectVolume;
    public Dropdown difficulty;

    private Hermes hermes;
	// Use this for initialization
	void Start () {
        InitializeSettings();
        hermes = Hermes.GetInstance();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    
    public void InitializeSettings()
    {
        var gameState = FindObjectOfType<GameState>();
        if(gameState!=null)
        {
            musicVolume.value = gameState.gameData.musicVolume;
            effectVolume.value = gameState.gameData.effectsVolume;
            difficulty.value = gameState.gameData.difficulty;
        }
    }

    public void SaveSettings()
    {
        var gameState = FindObjectOfType<GameState>();
        if (gameState != null)
        {
            gameState.gameData.musicVolume = musicVolume.value;
            gameState.gameData.effectsVolume = effectVolume.value;
            gameState.gameData.difficulty = difficulty.value;

            gameState.SaveDataToFile();
        }
        hermes.PostNotification(this, "GoToMainMenu");
    }

}
