﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DifficultyDropdownInit : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Dropdown dropdown = GetComponent<Dropdown>();
        List<string> options = new List<string>
        {
            LocalizationManager.GetLocalizedValue("settings.difficulty.low"),
            LocalizationManager.GetLocalizedValue("settings.difficulty.medium"),
            LocalizationManager.GetLocalizedValue("settings.difficulty.high")
        };
        dropdown.AddOptions(options);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

