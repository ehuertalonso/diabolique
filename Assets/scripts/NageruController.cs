﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NageruController : MonoBehaviour {

    public float velocity;


    public void MovePlayer(Vector3 displacement) {
        displacement.Normalize();
        gameObject.GetComponent<Rigidbody2D>().velocity = displacement * velocity * Time.deltaTime;
    }

}
