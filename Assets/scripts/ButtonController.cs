﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {

    private Hermes hermes;
    void Start () {
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "GoToMainMenu");
    }
	
	public void GoToMainMenu()
    {
        hermes.PostNotification(this, "StopAll");
        hermes.PostNotification(this, "LoadLevel", "welcome");
        hermes.PostNotification(this, "LoadSceneLoaded");
        hermes.PostNotification(this, "RequestBanner");
        hermes.PostNotification(this, "RequestInterstitial");
    }

    public void GoToMainMenuAndShowAd()
    {
        hermes.PostNotification(this, "StopAll");
        hermes.PostNotification(this, "RemoveBanner");
        hermes.PostNotification(this, "LoadLevel", "welcome");
        hermes.PostNotification(this, "ShowAdEndOfGame");
        hermes.PostNotification(this, "RequestBanner");
        hermes.PostNotification(this, "RequestInterstitial");
    }

    public void Play()
    {
        hermes.PostNotification(this, "StopAll");
        hermes.PostNotification(this, "RemoveBanner");
        hermes.PostNotification(this, "LoadLevel", "Play");
        hermes.PostNotification(this, "StartGame");
    }

    public void PlayAgain()
    {
        hermes.PostNotification(this, "StopAll");
        hermes.PostNotification(this, "RemoveBanner");
        hermes.PostNotification(this, "LoadLevel", "Play");
        hermes.PostNotification(this, "ShowAdEndOfGame");
        hermes.PostNotification(this, "RequestInterstitial");
        hermes.PostNotification(this, "StartGame");
    }

    public void LoadSettings()
    {
        hermes.PostNotification(this, "RemoveBanner");
        hermes.PostNotification(this, "LoadLevel", "settings");
    }

    public void LoadAbout()
    {
        hermes.PostNotification(this, "LoadLevel", "about");
    }

    public void ShowGPGScores()
    {
        if (!Social.localUser.authenticated)
        {
            hermes.PostNotification(this, "GPGConnect");
        }
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(GPGController.leaderboard_diabolique_scoreboard);
        }
    }
}
