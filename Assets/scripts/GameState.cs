﻿using GooglePlayGames;
using GooglePlayGames.BasicApi;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SocialPlatforms;

public class GameState : MonoBehaviour {

    public string preferenceFileName;
    public GameData gameData;
    public GameManager gameManager;

    private int _score;
    private int key;
    private int score
    {
        get { return _score ^ key; }
        set
        {
            key = UnityEngine.Random.Range(0, int.MaxValue-1);
            _score = value ^ key;
        }
    }
    private float timeStart;
    private float timeEnds;
    private static bool created = false;
    private string filePath;
    private Hermes hermes;
    private bool checkDisconnection = false;

    private void Awake()
    {
        if (preferenceFileName == "") filePath = Application.persistentDataPath + "/data.dat";
        else filePath = Application.persistentDataPath + "/" + preferenceFileName;
        if (!created)
        {
            DontDestroyOnLoad(gameObject);
            created = true;
        }
        else Destroy(gameObject);
    }

    private void Update()
    {
        if(!Social.localUser.authenticated && checkDisconnection)
        {
            // when player disconnects reset userid and maxscore
            gameData.userId = "";
            gameData.maxScore = 0;
            checkDisconnection = false;
        }
    }

    // Use this for initialization
    void Start ()
    {
        hermes = Hermes.GetInstance();
        score = 0;
        gameData = new GameData()
        {
            musicVolume = 50,
            effectsVolume = 50,
            difficulty = 0,
            maxScore = 0,
            userId = ""
        };
        LoadDataFromFile();
        gameManager = FindObjectOfType<GameManager>();
        PlayGamesPlatform.Activate();
        hermes.AddObserver(this, "CheckHighScoreAndReport");
        hermes.AddObserver(this, "GPGConnect");

        hermes.PostNotification(this,"GPGConnect");
    }

    public void GPGConnect(Notification notification)
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                checkDisconnection = true;
                // If switching users load score from leader board and don't share offline score
                if(gameData.userId != "" && gameData.userId != Social.localUser.id)
                {
                    gameData.maxScore = 0;
                }
                gameData.userId = Social.localUser.id;
                hermes.PostNotification(this, "EnableScoreButton");
                hermes.PostNotification(this, "CheckHighScoreAndReport");
            }
            else
            {
                hermes.PostNotification(this, "DisableScoreButton");
            }
        });
    }


    void CheckHighScoreAndReport()
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.LoadScores(
                GPGController.leaderboard_diabolique_scoreboard,
                LeaderboardStart.PlayerCentered,
                1,
                LeaderboardCollection.Public,
                LeaderboardTimeSpan.AllTime,
            (LeaderboardScoreData data) =>
            {
                if (data.PlayerScore.value > gameData.maxScore)
                {
                    gameData.maxScore = data.PlayerScore.value;
                    SaveDataToFile();
                }
            // If playing offine we got a higher score, report it now
            else if (gameData.maxScore > data.PlayerScore.value)
                {
                    Social.ReportScore(gameData.maxScore, GPGController.leaderboard_diabolique_scoreboard,
                    (bool ok) =>
                    {

                    });
                }

            });
        }
    }

    public void StartGame()
    {
        score = 0;
        timeStart = Time.time;
    }

    public void EndGame()
    {
        timeEnds = Time.time;
        AddScore(Mathf.FloorToInt((timeEnds - timeStart) * gameManager.GetScorePerTimeFactor()));

    }

    public void AddScore(int amount)
    {
        score += amount;
    }

    public long CurrentScore()
    {
        return score;
    }

    public void SaveDataToFile()
    {
        BinaryFormatter bf = new BinaryFormatter();
        FileStream fs = File.Open(filePath, FileMode.Create);

        bf.Serialize(fs,gameData);
        fs.Close();
        // If preferences change restore values
        gameManager.IncreaseParametersWithDifficulty();
    }

    private void LoadDataFromFile()
    {
        if (File.Exists(filePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream fs = File.Open(filePath, FileMode.Open);

            gameData = (GameData)bf.Deserialize(fs);

            fs.Close();
        }
    }

    public void SetMaxScore()
    {
        if (score > gameData.maxScore)
        {
            gameData.maxScore = score;
            SaveDataToFile();
        }
        // Report only if new max score
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(score, GPGController.leaderboard_diabolique_scoreboard,
                (bool success) =>
                {

                });
        }
    }

    [Serializable]
    public class GameData
    {
        public float musicVolume;
        public float effectsVolume;
        public int difficulty;
        public long maxScore;
        public string userId;
    }

}

