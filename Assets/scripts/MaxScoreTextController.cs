﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MaxScoreTextController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameState gm = FindObjectOfType<GameState>();
        if(gm)
        {
            GetComponent<Text>().text = gm.gameData.maxScore.ToString();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
