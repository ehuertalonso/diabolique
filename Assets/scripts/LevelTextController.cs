﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelTextController : MonoBehaviour {

    GameManager gm;
    Text scoreText;
    public string localizedTextKey;
    // Use this for initialization
    void Start()
    {
        gm = FindObjectOfType<GameManager>();
        scoreText = GetComponent<Text>();
        if (gm)
        {
            scoreText.text = LocalizationManager.GetLocalizedValue(localizedTextKey) + gm.CurrentLevel().ToString();
        }
    }

    // Update is called once per frame
    void Update()
    {
        Refresh();
    }

    public void Refresh()
    {
        if (gm)
        {
            scoreText.text = LocalizationManager.GetLocalizedValue(localizedTextKey) + gm.CurrentLevel().ToString();
        }
    }
}
