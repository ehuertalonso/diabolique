﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    private static bool created = false;
    AudioSource musicSource;
    AudioSource effectSource;
    Hermes hermes;
    void Awake()
    {
        if (!created)
        {
            DontDestroyOnLoad(this.gameObject);
            created = true;
        }
        else Destroy(gameObject);
    }

    // Use this for initialization
    void Start () {
        musicSource = GameObject.FindWithTag("MusicSource").GetComponent<AudioSource>();
        effectSource = GameObject.FindWithTag("EffectSource").GetComponent<AudioSource>();
        hermes = Hermes.GetInstance();
        hermes.AddObserver(this, "PlayMusic");
        hermes.AddObserver(this, "StopMusic");
        hermes.AddObserver(this, "PlayEffect");
        hermes.AddObserver(this, "StopEffect");
        hermes.AddObserver(this, "StopAll");
	}

    void PlayMusic(Notification notification)
    {
        AudioOptions options = (AudioOptions)notification.data;
        string resource = options.resource;
        float volume = options.volume;
        musicSource.clip = Resources.Load<AudioClip>(resource);
        musicSource.volume = volume / 100;
        musicSource.loop = true;
        musicSource.Play();
    }

    void StopMusic()
    {
        musicSource.Stop();
    }

    void PlayEffect(Notification notification)
    {
        AudioOptions options = (AudioOptions)notification.data;
        string resource = options.resource;
        float volume = options.volume;
        effectSource.clip = Resources.Load<AudioClip>(resource);
        effectSource.volume = volume / 100;
        effectSource.loop = false;
        effectSource.Play();
    }

    void StopEffect()
    {
        effectSource.Stop();
    }

    void StopAll()
    {
        musicSource.Stop();
        effectSource.Stop();
    }
}

public class AudioOptions
{
    public string resource;
    public float volume;
}