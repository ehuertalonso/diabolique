﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreTextController : MonoBehaviour
{
    Text scoreText;
    GameState gs;
    public string localizedTextKey;

    // Use this for initialization
    void Start ()
    {
        gs = FindObjectOfType<GameState>(); 
        scoreText = GetComponent<Text>();
	}

    private void Update()
    {
        scoreText.text = LocalizationManager.GetLocalizedValue(localizedTextKey) + " " + gs.CurrentScore().ToString();
    }

}
