﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public float velocity;
    public float levelMaxTime;
    public int enemyDestroyedScore;

    private Vector3 velVector;
    private Vector3 lastUpdatePosition = Vector3.zero;
    private float NotBounceThreshold = 0.3f;
    private GameManager gm;
    private Hermes hermes;
    // Use this for initialization
    void Start () {
        velVector = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0f);
        velVector.Normalize();
        gameObject.GetComponent<Rigidbody2D>().velocity = velocity * velVector;
        gm = FindObjectOfType<GameManager>();
        Invoke("CheckBallBouncing", 1f);
        hermes = Hermes.GetInstance();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void CheckBallBouncing()
    {
        if(lastUpdatePosition!=Vector3.zero)
        {
            //not bouncing in x and next to wall
            if(Mathf.Abs(transform.position.x - lastUpdatePosition.x)<NotBounceThreshold && 
                (Mathf.Abs(transform.position.x - gm.worldLimits.limitLeft) < NotBounceThreshold || Mathf.Abs(transform.position.x - gm.worldLimits.limitRight) < NotBounceThreshold))
            {
                Vector3 currentVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
                GetComponent<Rigidbody2D>().velocity = new Vector3(Random.Range(-1f, 1f)*velocity, currentVelocity.y, 0);
            }
            //not bouncing in y and next to wall
            if (Mathf.Abs(transform.position.y - lastUpdatePosition.y) < NotBounceThreshold &&
                (Mathf.Abs(transform.position.y - gm.worldLimits.limitUp) < NotBounceThreshold || Mathf.Abs(transform.position.y - gm.worldLimits.limitBottom) < NotBounceThreshold))
            {
                Vector3 currentVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
                GetComponent<Rigidbody2D>().velocity = new Vector3(currentVelocity.x, Random.Range(-1f, 1f)*velocity, 0);
            }
        }
        lastUpdatePosition = transform.position;
        Invoke("CheckBallBouncing", 1f);
    }

    public void IncreaseVelocity(float amount)
    {
        Vector3 currentVelocity = gameObject.GetComponent<Rigidbody2D>().velocity;
        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector3(currentVelocity.x + amount, currentVelocity.y + amount,0);
    }

    public void OnCollisionEnter2D(Collision2D collision) {
        if (LayerMask.LayerToName(collision.gameObject.layer) == "Player") {
            hermes.PostNotification(this, "EndGame");
        }
    }

    private void OnDestroy()
    {
        gm.GetGameState().AddScore(enemyDestroyedScore);
    }
}
